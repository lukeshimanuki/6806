import tensorflow as tf
import numpy as np
import pdb

dtype = np.float16

print('a')
categoriesn = np.load('category_batches.npy')
bodiesn = np.load('body_batches.npy')
titlesn = np.load('title_batches.npy')
print('b')

categories = tf.get_variable('categories', categoriesn.shape, dtype=dtype)
print('c')
bodies = tf.get_variable('bodies', bodiesn.shape, dtype=dtype)
print('d')
titles = tf.get_variable('titles', titlesn.shape, dtype=dtype)
print('e')

saver = tf.train.Saver({'categories': categories, 'bodies': bodies, 'titles': titles}, True, max_to_keep=1)

sess = tf.Session()

print('f')
sess.run(tf.global_variables_initializer())

print('g')
sess.run([
	categories.assign(categoriesn),
])
print('h')
sess.run([
	bodies.assign(bodiesn),
])
print('i')
sess.run([
	titles.assign(titlesn),
])
print('j')

saver.save(sess, 'data_tensors')

pdb.set_trace()

