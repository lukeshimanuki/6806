import gzip
import pdb as pdb
import numpy as np
import pickle

train = [(int(query), {int(s) for s in similar.split(' ')}, {int(r) for r in random.split(' ')}) for row in open('askubuntu/train_random.txt', 'r') for query, similar, random in [row.split('\t')]]
pickle.dump(train, open('train.pickle', 'w'))

dev = [(int(query), {int(s.strip()) for s in similar.split(' ') if len(s) > 0}, {int(c.strip()) for c in candidates.split(' ')}, dict(zip([int(c.strip()) for c in candidates.split(' ')], [float(b) for b in bm25.split(' ')]))) for row in open('askubuntu/dev.txt', 'r') for query, similar, candidates, bm25 in [row.split('\t')]]
pickle.dump(dev, open('dev.pickle', 'w'))

test = [(int(query), {int(s.strip()) for s in similar.split(' ') if len(s) > 0}, {int(c.strip()) for c in candidates.split(' ')}, dict(zip([int(c.strip()) for c in candidates.split(' ')], [float(b) for b in bm25.split(' ')]))) for row in open('askubuntu/test.txt', 'r') for query, similar, candidates, bm25 in [row.split('\t')]]
pickle.dump(test, open('test.pickle', 'w'))

text_tokenized = [row for row in gzip.open('askubuntu/text_tokenized.txt.gz', 'r')]

indices = {int(row.split('\t')[0].strip()): i for i,row in enumerate(text_tokenized)}
pickle.dump(indices, open('indices.pickle', 'w'))

lengths = np.array([len(row.split('\t')[1].split(' ')) for row in text_tokenized], dtype=np.int32)
np.save('lengths.npy', lengths)

#embedding_file = open('glove_pruned.txt', 'r')
embedding_file = gzip.open('vectors_stackexchange.txt.gz', 'r')
embedding = {l[0].lower(): np.array([0.] + [float(v.strip()) for v in l[1:] if len(v.strip()) > 0], dtype=np.float16) for row in embedding_file for l in [row.split(' ')]}
embedding_size = embedding.items()[0][1].shape[0] - 1
unknown_embedding = np.array([1.] + [0.] * embedding_size, dtype=np.float16)

def embed(word):
	w = word.strip()
	# return embedding if exists
	# otherwise set first feature to 1
	if word.lower() in embedding:
		return embedding[word.lower()]
	else:
		return unknown_embedding

atext_tokenized = [row for row in gzip.open('Android/corpus.tsv.gz', 'r')]

aindices = {int(row.split('\t')[0].strip()): i for i,row in enumerate(atext_tokenized)}
pickle.dump(aindices, open('aindices.pickle', 'w'))

alengths = np.array([len(row.split('\t')[1].split(' ')) for row in atext_tokenized], dtype=np.int32)
np.save('alengths.npy', alengths)

max_length = lengths.max()
max_alength = alengths.max()

mask = np.stack([np.concatenate([np.ones(l, np.float16), np.zeros(max_length - l, np.float16)]) / l for l in lengths])
np.save('mask.npy', mask)

amask = np.stack([np.concatenate([np.ones(l, np.float16), np.zeros(max_alength - l, np.float16)]) / l for l in alengths])
np.save('amask.npy', amask)

abodies = np.stack(sum(embed(w) for w in body.split(' ')) for row in atext_tokenized for id, title, body in [row.split('\t')])
np.save('abodies.npy', abodies)

atitles_list = [title.split(' ') for row in atext_tokenized for id, title, body in [row.split('\t')]]
atitles_max_length = len(max(atitles_list, key=lambda title: len(title)))
atitles = np.zeros([len(atitles_list), atitles_max_length, embed('').shape[0]], dtype=np.float16)
for i,title in enumerate(atitles_list):
	features = np.stack(embed(word) for word in title)
	atitles[i,:features.shape[0],:] = features
np.save('atitles.npy', atitles)

bodies = np.stack(sum(embed(w) for w in body.split(' ')) for row in text_tokenized for id, title, body in [row.split('\t')])
np.save('bodies.npy', bodies)

titles_list = [title.split(' ') for row in text_tokenized for id, title, body in [row.split('\t')]]
titles_max_length = len(max(titles_list, key=lambda title: len(title)))
titles = np.zeros([len(titles_list), titles_max_length, embed('').shape[0]], dtype=np.float16)
for i,title in enumerate(titles_list):
	features = np.stack(embed(word) for word in title)
	titles[i,:features.shape[0],:] = features
np.save('titles.npy', titles)

pdb.set_trace()

