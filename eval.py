import numpy as np
import pickle
import pdb

from metrics import *

dtype = np.float32

for feature_name in ['features']:
#for feature_name in ['aefeatures']:
#for feature_name in ['features', 'aelatent', 'aefeatures']:
	print("using features: {}".format(feature_name))

	indices = pickle.load(open('indices.pickle', 'r'))
	features = np.load("{}.npy".format(feature_name))

	def similarity(a, b):
		return a.dot(b) / (np.linalg.norm(a) * np.linalg.norm(b))

	for dataset in ['dev', 'test']:
		data = pickle.load(open("{}.pickle".format(dataset), 'r'))

		list_baseline_metrics = []
		list_model_metrics = []

		for query, similar, candidates, bm25 in data:
			if len(similar) == 0:
				continue

			baseline_rank = sorted(candidates, key=lambda c: -bm25[c])
			baseline_metrics = compute_metrics(baseline_rank, similar)
			list_baseline_metrics.append(baseline_metrics)

			model_rank = sorted(candidates, key=lambda c: -similarity(features[indices[query]], features[indices[c]]))
			model_metrics = compute_metrics(model_rank, similar)
			list_model_metrics.append(model_metrics)

		print("{} baseline:\t{}".format(dataset, avg(list_baseline_metrics)))
		print("{} model:\t{}".format(dataset, avg(list_model_metrics)))

	import meter

	for dataset in ['dev', 'test']:
		data = pickle.load(open("{}.pickle".format(dataset), 'r'))
		auc = meter.AUCMeter()

		pairs = [(d[0], s) for d in data for s in d[1]]
		sim = np.array([similarity(features[indices[a]], features[indices[b]]) for a,b in pairs], dtype=dtype)
		targ = np.ones(sim.shape, dtype=dtype)
		auc.add(sim, targ)

		npairs = [(d[0], r) for d in data for r in d[2] - d[1]]
		nsim = np.array([similarity(features[indices[a]], features[indices[b]]) for a,b in npairs], dtype=dtype)
		ntarg = np.zeros(nsim.shape, dtype=dtype)
		auc.add(nsim, ntarg)

		print("{}\t{}".format(dataset, auc.value(.05)))

		auc = meter.AUCMeter()
		for q, sim, cand, bm in data:
			for c in cand:
				auc.add(np.array([bm[c]]), np.array([1 if c in sim else 0]))
		print("bm25: {}\t{}".format(dataset, auc.value(.05)))

	aindices = pickle.load(open('aindices.pickle', 'r'))
	afeatures = np.load("a{}.npy".format(feature_name))

	print('android:')
	for dataset in ['dev', 'test']:
		auc = meter.AUCMeter()
		for label in [0, 1]:
			filename = "Android/{}.{}.txt".format(dataset, 'pos' if label == 1 else 'neg')
			file = open(filename, 'r')
			pairs = [(int(id) for id in row.split(' ')) for row in file]
			sim = np.array([similarity(afeatures[aindices[a]], afeatures[aindices[b]]) for a,b in pairs], dtype=dtype)
			targ = np.ones(sim.shape, dtype=dtype) * label
			auc.add(sim, targ)
		print("{}\t{}".format(dataset, auc.value(.05)))

	from sklearn.feature_extraction.text import TfidfVectorizer
	import gzip
	tfidf = TfidfVectorizer(min_df=1, max_features=None)
	#tfidf = TfidfVectorizer(input='content', encoding='utf-8', decode_error='strict', strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, analyzer='word', stop_words=None, token_pattern='(?u)\b\w\w+\b', ngram_range=(1, 1), max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False, dtype=np.float32, norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=False)
	atitles_str = [row.split('\t')[1] for row in gzip.open('Android/corpus.tsv.gz', 'r')]
	atfidffeatures = tfidf.fit_transform(atitles_str)
	for dataset in ['dev', 'test']:
		auc = meter.AUCMeter()
		for label in [0, 1]:
			filename = "Android/{}.{}.txt".format(dataset, 'pos' if label == 1 else 'neg')
			file = open(filename, 'r')
			pairs = [(int(id) for id in row.split(' ')) for row in file]
			sim = np.array([similarity(atfidffeatures[aindices[a]].A.reshape([-1]), atfidffeatures[aindices[b]].A.reshape([-1])) for a,b in pairs], dtype=dtype)
			targ = np.ones(sim.shape, dtype=dtype) * label
			auc.add(sim, targ)
		print("tfidf: {}\t{}".format(dataset, auc.value(.05)))

#pdb.set_trace()

