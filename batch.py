import numpy as np
import pickle
import pdb

batch_size = 20
dtype = np.float16

clusters = pickle.load(open('clusters.pickle', 'r'))
indices = pickle.load(open('indices.pickle', 'r'))
titles = np.load('titles.npy')
bodies = np.load('bodies.npy')

#if os.path.isfile('params.npz'):
#	params = np.loadz('params.npz')

batches = []
all_random = list(clusters[0])
rand_idx = 0
for cluster in clusters[1:-200]:
	num_random = batch_size - len(cluster)
	if rand_idx + num_random >= len(all_random):
		rand_idx = 0
	random = all_random[rand_idx:rand_idx+num_random]
	rand_idx += num_random

	batch = list(cluster) + random

	t = np.stack([titles[indices[i]] for i in batch])
	b = np.stack([bodies[indices[i]] for i in batch])

	# all questions with a 1 are similar with each other
	# questions with a 0 are dissimilar with everything
	categories = np.concatenate([np.ones(len(cluster), dtype=dtype), np.zeros(num_random, dtype=dtype)])

	batches.append((t, b, categories))

title_batches = np.stack([t for t,b,c in batches])
body_batches = np.stack([b for t,b,c in batches])
category_batches = np.stack([c for t,b,c in batches])

np.save('title_batches.npy', title_batches)
np.save('body_batches.npy', body_batches)
np.save('category_batches.npy', category_batches)

pdb.set_trace()

