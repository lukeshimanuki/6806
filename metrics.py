import pdb

def avg(l):
	if len(l) == 0:
		return []

	num_components = len(l[0])

	return [float(sum(a[i] for a in l)) / len(l) for i in range(num_components)]

def p_at_x(rank, ground_truth, x):
	return float(len(set(rank[:x]) & ground_truth)) / x

def reciprocal_rank(rank, ground_truth):
	for i, r in enumerate(rank):
		if r in ground_truth:
			return 1. / (i + 1)
	print('error: reciprocal_rank')
	pdb.set_trace()

def compute_metrics(rank, ground_truth):
	ap = sum(p_at_x(rank, ground_truth, i + 1) for i in range(len(rank)) if rank[i] in ground_truth) / len(ground_truth)
	rr = reciprocal_rank(rank, ground_truth)
	p1 = p_at_x(rank, ground_truth, 1)
	p5 = p_at_x(rank, ground_truth, 5)

	return [ap, rr, p1, p5]

