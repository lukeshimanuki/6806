# 
# util functions
#
import torch
from torch.autograd import Variable

def mask_output(h,mask):
    mask=Variable(torch.stack([mask for _ in range(h.size()[2])],dim=2), requires_grad=False)
    return h*mask

def mean_pool(h,mask):
    divisor=Variable(torch.stack([mask.sum(0) for _ in range(h.size()[2])],dim=1),requires_grad=False)
    return mask_output(h,mask).sum(dim=0).div(divisor+1e-6)
