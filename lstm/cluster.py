import pickle
import pdb

train = pickle.load(open('data/u_train.pkl', 'rb'))
train_indices = {id: i for i,(id,_,_) in enumerate(train)}

for t in train:
	for t2 in t[1]:
		if t2 in train_indices:
			train[train_indices[t2]][1].add(t[0])
		else:
			train_indices[t2] = len(train)
			train.append((t2, {t[0]}, {}))

clustered = set()
clusters = []

for query, similar, random in train:
	if query in clustered:
		continue

	i = {query}
	s = list(similar)
	while len(s) > 0:
		sim = s[-1]
		s.pop()
		if sim in clustered:
			pdb.set_trace()
		if sim not in i:
			i.add(sim)
			if sim in train_indices:
				_,similar2,_ = train[train_indices[sim]]
				s += list(similar2)

	clusters.append(i)
	clustered |= i

all_random = set.union(*[r for _,_,r in train])

clusters = sorted(clusters, key=lambda c: len(c))
clusters = [all_random - clustered] + clusters
pickle.dump(clusters, open('u_cluster.pkl', 'wb'))

print("done")
