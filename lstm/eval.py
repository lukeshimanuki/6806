import numpy as np
import pickle

import torch
from torch import nn, optim, utils
import torch.nn.functional as F
from torch.autograd import Variable

indices = pickle.load(open('data/corpus/u_index.pkl', 'rb'))

def similarity(a, b):
    return a.dot(b) / (np.linalg.norm(a) * np.linalg.norm(b))

def avg(l):
    if len(l) == 0:
        return []

    num_components = len(l[0])

    return [float(sum(a[i] for a in l)) / len(l) for i in range(num_components)]

def p_at_x(rank, ground_truth, x):
    return float(len(set(rank[:x]) & ground_truth)) / x

def reciprocal_rank(rank, ground_truth):
    for i, r in enumerate(rank):
        if r in ground_truth:
            return 1. / (i + 1)
    print('error: reciprocal_rank')
    pdb.set_trace()

def compute_metrics(rank, ground_truth):
    ap = sum(p_at_x(rank, ground_truth, i + 1) for i in range(len(rank)) if rank[i] in ground_truth) / len(ground_truth)
    rr = reciprocal_rank(rank, ground_truth)
    p1 = p_at_x(rank, ground_truth, 1)
    p5 = p_at_x(rank, ground_truth, 5)

    return [ap, rr, p1, p5]

features=pickle.load(open("features3.pkl","rb"))

for dataset in ['dev','test']:
    data = pickle.load(open("data/datasets/u_{}.pkl".format(dataset), 'rb'))

    list_baseline_metrics = []
    list_model_metrics = []

    for query, similar, candidates, bm25 in data:
        if len(similar) == 0:
            continue

        baseline_rank = sorted(candidates, key=lambda c: -bm25[c])
        baseline_metrics = compute_metrics(baseline_rank, similar)
        list_baseline_metrics.append(baseline_metrics)

        model_rank = sorted(candidates, key=lambda c: -similarity(features[query].data.squeeze().numpy(), features[c].data.squeeze().numpy()))
        model_metrics = compute_metrics(model_rank, similar)
        list_model_metrics.append(model_metrics)

    print("{} baseline:\t{}".format(dataset, avg(list_baseline_metrics)))
    print("{} model:\t{}".format(dataset, avg(list_model_metrics)))

