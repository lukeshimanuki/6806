import numpy as np
import pickle, itertools, random

titles = np.load('data/u_title.npy')
bodies = np.load('data/u_body.npy')
# atitles = np.load('data/a_title.npy')
# abodies = np.load('data/a_body.npy')
clusters_l = pickle.load(open('u_cluster.pkl', 'rb'))
clusters = [list(c) for c in clusters_l[1:]]
random_examples = list(clusters_l[0])
indices = pickle.load(open('data/u_index.pkl', 'rb'))


BATCH_SIZE=25
dataset=pickle.load(open("data/u_data.pkl",'rb'))
print("dataset loaded")
random.shuffle(dataset)
print("dataset shuffled")
print("length of dataset:",len(dataset))
NUM_BATCHES=len(dataset)//BATCH_SIZE


title_batches=np.stack([np.stack([np.stack([titles[indices[q_id]] for q_id in sample]) for sample in dataset[i*BATCH_SIZE:(i+1)*BATCH_SIZE]]) for i in range(NUM_BATCHES)])
np.save('u_title_batch.npy', title_batches)
print("done title batches",title_batches.shape)
# (271, 25, 22, 38, 201)
# num_batches, batch_size, len(q,p+,p) , word length, word_embed_len


body_batches=np.stack([np.stack([np.stack([bodies[indices[q_id]] for q_id in sample]) for sample in dataset[i*BATCH_SIZE:(i+1)*BATCH_SIZE]]) for i in range(NUM_BATCHES)])
np.save('u_body_batch.npy', body_batches)
print("done body batches",body_batches.shape)
# (271, 25, 22, MAX_BODY_LENGTH, 201)
