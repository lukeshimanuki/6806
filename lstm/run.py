import pickle
import numpy as np

import torch
from torch import nn
from torch.autograd import Variable

from utils import mean_pool


titles=np.load("data/corpus/u_title.npy")
indices=pickle.load(open("data/corpus/u_index.pkl","rb"))

NUM_LAYERS=1
HIDDEN_SIZE=240
input_size=201
lstm=nn.LSTM(input_size,HIDDEN_SIZE,NUM_LAYERS)
lstm=torch.load("trained_models/model5_random.trc")
# lstm.load_state_dict(torch.load("trained_models/model5_random.trc"))

feat={}
for dataset in ['dev','test']:
    data = pickle.load(open("data/datasets/u_{}.pkl".format(dataset), 'rb'))

    for query, similar, candidates, bm25 in data:
        for c in [query]+list(candidates):
            if c not in feat:
                mask=torch.from_numpy(titles[indices[query]][:,0:1].astype("float")).float()
                X=Variable(torch.from_numpy(titles[indices[query]].astype("float")).float().unsqueeze(1)) 

                h0=Variable(torch.FloatTensor(NUM_LAYERS,1,HIDDEN_SIZE).zero_())
                c0=Variable(torch.FloatTensor(NUM_LAYERS,1,HIDDEN_SIZE).zero_())

                h, (_,_) = lstm(X, (h0,c0))
                h=mean_pool(h,mask)
                feat[c]=h.float()

pickle.dump(feat, open('features5.pkl', 'wb'))