# 2017-12-07
# 6.806
#
import pickle
import numpy as np

import torch
from torch import nn, optim, utils
import torch.nn.functional as F
from torch.autograd import Variable

from utils import mean_pool

title_batches=np.load('data/batches/u_title_batch.npy')
# body_batches=np.load('u_body_batch.npy')
title_batches=torch.from_numpy(title_batches.astype("float")[51:59]).float().permute(0,2,3,1,4)

print("title_batches.shape:",title_batches.shape)

# print(title_batches[53,0,:,:,:])
# batch 53 question 0

# PARAMS
lr=1e-5
wd=1e-3

input_size=201
HIDDEN_SIZE=240
NUM_LAYERS=1
NUM_EPOCHS=30

lstm=nn.LSTM(input_size,HIDDEN_SIZE,NUM_LAYERS)
criterion=nn.MultiMarginLoss(margin=.2)
optimizer=optim.Adam(lstm.parameters(), lr=lr, weight_decay=wd)

(num_batches,sample22,word38,bs25,embed200)=title_batches.size()

for epoch in range(NUM_EPOCHS):
    for batch_i in range(0, num_batches):
        states=[]
        for question_i in range(sample22):
            question_batch=title_batches[batch_i,question_i,:,:,:] 

            mask=question_batch[:,:,0]
            batch=question_batch[:,:,:]

            h0=Variable(torch.FloatTensor(NUM_LAYERS,bs25,HIDDEN_SIZE).zero_())
            c0=Variable(torch.FloatTensor(NUM_LAYERS,bs25,HIDDEN_SIZE).zero_())

            print("batch",batch_i,"question",question_i)
            
            X=Variable(batch)
            print("X before lstm",X)
            # print(list(lstm.parameters()))
            h, (_,_) = lstm(X, (h0,c0))

            print(h.size(),mask.size())
            print("h before meanpool",h)
            h=mean_pool(h,mask)
            print(h)
            states.append(h)

        q=torch.stack([states[0] for _ in range(sample22-1)],dim=1)
        p=torch.stack(states[1:],dim=1)
        sims=F.cosine_similarity(q,p,dim=2)
        print("SIM!!",sims)
        y=Variable(torch.LongTensor(bs25).zero_(), requires_grad=False)

        loss=criterion(sims,y)
        print(loss)

        for p in lstm.parameters():
            print(p.grad)
        optimizer.zero_grad()
        loss.backward()
        for p in lstm.parameters():
            print(p.grad)
        # optimizer.step()
        for p in rbm.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    print("epoch",epoch,"loss",loss.data)
    print(sims)

torch.save(lstm.state_dict(), "trained_models/model6_240.trc")