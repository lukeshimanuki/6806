import gzip
import pdb as pdb
import numpy as np
import pickle

np.set_printoptions(threshold=np.nan, precision=4)

train = [(int(query), {int(s) for s in similar.split(' ')}, {int(r) for r in random.split(' ')}) for row in open('askubuntu/train_random.txt', 'r') for query, similar, random in [row.split('\t')]]
pickle.dump(train, open('data/u_train.pkl', 'wb'))

dev = [(int(query), {int(s.strip()) for s in similar.split(' ') if len(s) > 0}, {int(c.strip()) for c in candidates.split(' ')}, dict(zip([int(c.strip()) for c in candidates.split(' ')], [float(b) for b in bm25.split(' ')]))) for row in open('askubuntu/dev.txt', 'r') for query, similar, candidates, bm25 in [row.split('\t')]]
pickle.dump(dev, open('data/u_dev.pkl', 'wb'))

test = [(int(query), {int(s.strip()) for s in similar.split(' ') if len(s) > 0}, {int(c.strip()) for c in candidates.split(' ')}, dict(zip([int(c.strip()) for c in candidates.split(' ')], [float(b) for b in bm25.split(' ')]))) for row in open('askubuntu/test.txt', 'r') for query, similar, candidates, bm25 in [row.split('\t')]]
pickle.dump(test, open('data/u_test.pkl', 'wb'))

with gzip.open('askubuntu/text_tokenized.txt.gz', 'r') as g:
	text_tokenized = [row.decode("utf-8") for row in g]

	indices = {int(row.split('\t')[0].strip()): i for i,row in enumerate(text_tokenized)}
	pickle.dump(indices, open('data/index.pkl', 'wb'))

embedding_size = 200
with gzip.open('askubuntu/vector/vectors_pruned.200.txt.gz', 'rb') as gv:
	rows=[row.split(" ") for row in gv.read().decode().splitlines()]
	embedding = {row[0]: np.array([1.] + [float(v.strip()) for v in row[1:] if len(v) > 0], dtype=np.float16) for row in rows}

def embed(word):
	w = word.strip()
	# return embedding if exists
	# otherwise set first feature to 1
	if word in embedding:
		return embedding[word]
	else:
		return np.array([0.] + [0.] * embedding_size, dtype=np.float16)

print("embedding bodies")
# EMBED
MAX_BODY_LENGTH=50
bodies_list = [title.split(' ')[:MAX_BODY_LENGTH] for row in text_tokenized for id, title, body in [row.split('\t')]]
bodies_max_length = MAX_BODY_LENGTH#len(max(titles_list, key=lambda title: len(title)))
bodies = np.zeros([len(bodies_list), bodies_max_length, embed('').shape[0]], dtype=np.float16)
for i,body in enumerate(bodies_list):
	features = np.stack(embed(word) for word in body)
	bodies[i,:features.shape[0],:] = features

# BOW
# bodies = np.stack(sum(embed(w) for w in body.split(' ')) for row in text_tokenized for id, title, body in [row.split('\t')])
np.save('data/u_body.npy', bodies)

print("embedding titles")
titles_list = [title.split(' ') for row in text_tokenized for id, title, body in [row.split('\t')]]
titles_max_length = len(max(titles_list, key=lambda title: len(title)))
titles = np.zeros([len(titles_list), titles_max_length, embed('').shape[0]], dtype=np.float16)
for i,title in enumerate(titles_list):
	features = np.stack(embed(word) for word in title)
	titles[i,:features.shape[0],:] = features

np.save('data/u_title.npy', titles)

print("done")
pdb.set_trace()

