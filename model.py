import tensorflow as tf
import numpy as np
import pdb

filter_size = 3
hidden_size = 900
ae_size = 300
model_size = 10
padding = 'SAME'
aepool = 3
dtype = np.float32

# title: kxmxn
# body: kxn
def model(titles, mask, dropout, params=None):
	convW = tf.Variable(np.zeros([filter_size, titles.shape[2], hidden_size]), dtype=dtype)
	convB = tf.Variable(np.zeros([1, hidden_size]), dtype=dtype)

	if params is not None:
		convW = params[0]

	titles = tf.nn.dropout(titles, dropout)

	conv = tf.nn.convolution(titles, convW, padding) + convB

	pool = tf.reduce_sum(tf.reshape(mask, [tf.shape(mask)[0], tf.shape(mask)[1], 1]) * tf.tanh(conv), 1)

	pool = tf.nn.dropout(pool, dropout)

	#cat = tf.concat([pool, bodies], 1)

	#fcW = tf.Variable(np.zeros([cat.shape[1], model_size]), dtype=dtype)

	#fc = tf.tanh(tf.matmul(cat, fcW))

	return pool, [convW, convB]

# x: kxmodel_size
def similarity(x):
	norm = tf.norm(x, axis=1, keep_dims=True) + 1e-6
	return tf.matmul(x, tf.transpose(x)) / tf.matmul(norm, tf.transpose(norm))

	#sx = tf.shape(x)
	#x1 = tf.reshape(x, [sx[0], 1, sx[1]])
	#x2 = tf.reshape(x, [1, sx[0], sx[1]])
	#return -tf.reduce_sum(tf.pow(x1 - x2, 2), 2)

# x: kxmodel_size
def discriminate(x, params=None):
	fcW = tf.Variable(np.zeros([x.shape[1], 1], dtype=dtype))

	if params is not None:
		fcW = params[0]

	linear = tf.matmul(x, fcW)

	act = tf.nn.sigmoid(linear)

	return act, [fcW]

def autoencoder(titles, mask, dropout, params=None):
	convW = tf.Variable(np.zeros([filter_size, titles.shape[2], ae_size]), dtype=dtype)

	if params is not None:
		convW = params[0]

	titles = tf.nn.dropout(titles, dropout)

	latent = tf.tanh(tf.nn.convolution(titles, convW, padding))

	pool = tf.nn.pool(
		latent,
		window_shape=[aepool],
		pooling_type='AVG',
		padding='SAME',
	)

	latentd = tf.nn.dropout(pool, dropout)
	reconstruction = tf.tanh(tf.nn.convolution(latentd, tf.transpose(convW, [0,2,1]), padding))

	loss = tf.reduce_mean(tf.reduce_sum(tf.reshape(mask, [tf.shape(mask)[0], tf.shape(mask)[1], 1]) * tf.pow(reconstruction - titles, 2), 1))

	return latent, loss, [convW]

# flattened is true if titles is preflattened features
def flat(titles, mask, dropout, flattened, params=None):
	pool = titles

	if not flattened:
		pool = tf.reduce_sum(tf.reshape(mask, [tf.shape(mask)[0], tf.shape(mask)[1], 1]) * titles, 1)

	fcW = tf.Variable(np.zeros([pool.shape[1], model_size]), dtype=dtype)
	fcB = tf.Variable(np.zeros([1, model_size]), dtype=dtype)

	if params is not None:
		fcB = params[0]
		fcW = params[1]

	pool = tf.nn.dropout(pool, dropout)

	fc = tf.tanh(tf.matmul(pool, fcW) + fcB)

	#return pool * fcB, [fcB]
	return fc, [fcB, fcW]

# flattened is true if titles is preflattened features
def flat_sim(titles, mask, dropout, flattened, params=None):
	pool = titles

	if not flattened:
		pool = tf.reduce_sum(tf.reshape(mask, [tf.shape(mask)[0], tf.shape(mask)[1], 1]) * titles, 1)

	fcW = tf.Variable(np.zeros([pool.shape[1], model_size]), dtype=dtype)

	if params is not None:
		fcW = params[0]

	pool = tf.nn.dropout(pool, dropout)

	cat = tf.concatenate(pool, )

	fc = tf.tanh(tf.matmul(pool, fcW))

	norm = tf.norm(fc, axis=1, keep_dims=True) + 1e-6
	sim = tf.matmul(fc, tf.transpose(fc)) / tf.matmul(norm, tf.transpose(norm))

	return sim, [fcW]

