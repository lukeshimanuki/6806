import tensorflow as tf
import numpy as np
import os
import pdb
import sys
import pickle
import random

lr = 1e-5
aelr = 0e-3
aeflr = 0e-3
disc_lr = 1e-4
adversarial_lr = 1e-6
delta = .2
dropout = .5
num_epochs = 10000
beg_cluster = 0
end_clusters = -1
init_scalar = .1
num_random = 20
use_adversarial = True
train_ae = False
num_disc = 20
input_features = 'aecfeatures'
load = True
save = True
save_all = True
use_eval = True
eval_freq = 1
eval_downscale = 1
aef_only = False
print_initial_loss = False

dtype = np.float32

# 1 is askubuntu, 0 is Android

random_zeros = np.zeros(num_random, dtype=dtype)

def to_list(x):
	return [
		x[i]
		for i in range(x.shape[0])
	]

#categoriest = tf.get_variable('categories', np.load('category_batches.npy').shape, dtype=dtype)
#bodiest = tf.get_variable('bodies', np.load('body_batches.npy').shape, dtype=dtype)
#titlest = tf.get_variable('titles', np.load('title_batches.npy').shape, dtype=dtype)
#
#saver = tf.train.Saver({'categories': categoriest}, True, max_to_keep=1)

sess = tf.Session()

#saver.restore(sess, 'data_tensors')

#categories = to_tensor_list(categoriest)
#bodies = to_tensor_list(bodiest)
#titles = to_tensor_list(titlest)

#categories = to_list(np.load('category_batches.npy'))
#bodies = to_list(np.load('body_batches.npy'))
#titles = to_list(np.load('title_batches.npy'))

titles = np.load("{}.npy".format(input_features))
mask = np.load('mask.npy')
amask = np.load('amask.npy')
bodies = np.load('bodies.npy').astype(np.int)
atitles = np.load("a{}.npy".format(input_features))
abodies = np.load('abodies.npy')
clusters_l = pickle.load(open('clusters.pickle', 'r'))
clusters = [list(c) for c in clusters_l[1:]]
random_examples = list(clusters_l[0])
indices = pickle.load(open('indices.pickle', 'r'))

aelatent_np = np.load('aelatent.npy')

import model

def max_margin_loss(sim, cat):
	catT = tf.reshape(cat, [-1, 1])
	both = tf.matmul(catT, tf.transpose(catT))

	# q, p+, p
	d = tf.shape(sim)[0]
	margin = tf.reshape(sim, [d,1,d]) - tf.reshape(sim, [d,d,1]) + delta * (1 - tf.reshape(both, [1,d,d]))

	# q, p+
	max_margin = tf.reduce_max(margin, 2)

	# use each similar pair
	ident = tf.eye(tf.shape(sim)[0], dtype=dtype)
	mask = both - (both * ident)
	return tf.reduce_sum(max_margin * mask) / tf.reduce_sum(mask)

# high loss when similar questions score low
# or when dissimilar questions score high
# sim: batch_size x batch_size
# cat: batch_size
def balanced_loss(sim, cat):
	# convert to [0,1]
	pred = tf.clip_by_value((sim + 1) / 2, 1e-3, 1 - 1e-3)

	catT = tf.reshape(cat, [-1, 1])

	# mask for both positive
	both = tf.matmul(catT, tf.transpose(catT))

	# mask for one positive one negative
	one = tf.matmul(1 - catT, tf.transpose(catT)) + tf.matmul(catT, 1 - tf.transpose(catT))

	# lowest score of true positives
	pos = tf.reduce_min(both * sim + (1 - both) * 1)
	pos = tf.clip_by_value((pos + 1) / 2, 1e-3, 1 - 1e-3)

	# highest score of true negatives
	neg = tf.reduce_max(one * sim + (1 - one) * -1)
	neg = tf.clip_by_value((neg + 1) / 2, 1e-3, 1 - 1e-3)

	return max_margin_loss(sim, cat)

	# similarity should be high for pos
	# should be low for neg
	#return (tf.log(neg) - tf.log(pos)) / 2

	# similarity should be high when both
	# should be low when one
	#return -tf.reduce_sum(tf.log(pred) * both / tf.reduce_sum(both) + tf.log(1 - pred) * one / tf.reduce_sum(one)) / 2

input_titles = tf.placeholder(dtype, [None] + list(titles.shape[1:]))
input_mask = tf.placeholder(dtype, [None] + list(mask.shape[1:]))
#input_bodies = tf.placeholder(dtype, [None] + list(bodies.shape[1:]))
target_categories = tf.placeholder(dtype, [None])

features, params = model.model(input_titles, input_mask, dropout)

sim = model.similarity(features)

l = max_margin_loss(sim, target_categories)
l = balanced_loss(sim, target_categories)
#l = loss(sim, target_categories)

#opt = tf.train.AdamOptimizer(lr, epsilon=1e-4).minimize(l, var_list=params)
opt = tf.train.AdamOptimizer(lr).minimize(l, var_list=params)

### adversarial ###

input_dpos = tf.placeholder(dtype, [None] + list(titles.shape[1:]))
input_dneg = tf.placeholder(dtype, [None] + list(atitles.shape[1:]))
input_dpos_mask = tf.placeholder(dtype, [None] + list(mask.shape[1:]))
input_dneg_mask = tf.placeholder(dtype, [None] + list(amask.shape[1:]))

pos_features, _ = model.model(input_dpos, input_dpos_mask, dropout, params)
neg_features, _ = model.model(input_dneg, input_dneg_mask, dropout, params)

pos_prediction, dparams = model.discriminate(pos_features)
neg_prediction, _ = model.discriminate(neg_features, dparams)

dl = -tf.reduce_mean(tf.log(pos_prediction)) - tf.reduce_mean(tf.log(1 - neg_prediction))

dopt = tf.train.AdamOptimizer(disc_lr).minimize(dl, var_list=dparams)
aopt = tf.train.AdamOptimizer(adversarial_lr).minimize(-dl, var_list=params) if use_adversarial else tf.constant(0., dtype=dtype)

###

### autoencoder ###

_, aeloss_pos, aeparams = model.autoencoder(input_dpos, input_dpos_mask, dropout)
_, aeloss_neg, _ = model.autoencoder(input_dneg, input_dneg_mask, dropout, aeparams)
aeloss = (aeloss_pos + aeloss_neg) / 2
aeopt = tf.train.AdamOptimizer(aelr).minimize(aeloss, var_list=aeparams) if train_ae else tf.constant(0., dtype=dtype)

aelatent, _, _ = model.autoencoder(input_titles, input_mask, dropout, aeparams)
if aef_only:
	aelatent = tf.placeholder(dtype, [None] + list(aelatent_np.shape[1:]))
flattened = aef_only
aeffeatures, aefparams = model.flat(aelatent, input_mask, dropout, flattened)
aefsim = model.similarity(aeffeatures)
aefl = max_margin_loss(aefsim, target_categories)
aefl = balanced_loss(aefsim, target_categories)

aefopter = tf.train.AdamOptimizer(aeflr)
aefgrads_and_vars = aefopter.compute_gradients(aefl, aefparams)
aefclipped_grads_and_vars = [(tf.clip_by_norm(grad, 1e-1), var) for grad, var in aefgrads_and_vars]
aefopt = aefopter.apply_gradients(aefclipped_grads_and_vars)

###

### evaluation ###

from metrics import *
import meter

aindices = pickle.load(open('aindices.pickle', 'r'))
eval_data = {
	dataset:
	pickle.load(open("{}.pickle".format(dataset), 'r'))
	for dataset in ['dev', 'test']
}
aeval_data = {
	dataset: {
		label:
		[(int(a), int(b)) for row in open("Android/{}.{}.txt".format(dataset, 'pos' if label == 1 else 'neg'), 'r') for a,b in [row.split(' ')]]
		for label in [0, 1]
	}
	for dataset in ['dev', 'test']
}
eval_title = tf.placeholder(dtype, [2, None] + list(titles.shape[2:]))
eval_mask = tf.placeholder(dtype, [2, None] + list(mask.shape[2:]))
model_encoder = model.model(eval_title, eval_mask, 1, params)[0]
ae_encoder = tf.reduce_sum(model.autoencoder(eval_title, eval_mask, 1, aeparams)[0], 1)
aef_encoder = model.flat(model.autoencoder(eval_title, eval_mask, 1, aeparams)[0], eval_mask, 1, False, aefparams)[0]
model_sim = tf.reduce_sum(model.similarity(model_encoder)[0,1])
ae_sim = tf.reduce_sum(model.similarity(ae_encoder)[0,1])
aef_sim = tf.reduce_sum(model.similarity(aef_encoder)[0,1])
def eval():
	for name, sim in [('m', model_sim)]:
	#for name, sim in [('aef', aef_sim)] if aef_only else [('m', model_sim), ('ae', ae_sim), ('aef', aef_sim)]:
		for dataset in ['dev']:
		#for dataset in ['dev', 'test']:
			data = eval_data[dataset]

			list_model_metrics = []

			for query, similar, candidates, bm25 in data:
				if len(similar) == 0:
					continue

				model_rank = sorted(candidates, key=lambda c: -sess.run(sim, {eval_title: titles[[indices[query], indices[c]]], eval_mask: mask[[indices[query], indices[c]]]}))
				model_metrics = compute_metrics(model_rank, similar)
				list_model_metrics.append(model_metrics)

			print("{}\t{}\t{}".format(name, dataset, avg(list_model_metrics)))

		for dataset in ['dev']:
		#for dataset in ['dev', 'test']:
			auc = meter.AUCMeter()
			for label in [0, 1]:
				pairs = aeval_data[dataset][label][::eval_downscale]
				asim = np.array([sess.run(sim, {eval_title: atitles[[aindices[a], aindices[b]]], eval_mask: amask[[aindices[a], aindices[b]]]}) for a,b in pairs], dtype=dtype)
				atarg = np.ones(asim.shape, dtype=dtype) * label
				auc.add(asim, atarg)
			print("a{}\t{}\t{}".format(name, dataset, auc.value(.05)))

###

aeopt = aefopt = tf.constant(0., dtype=dtype)

sess.run(tf.global_variables_initializer())

all_params = params + dparams + aeparams + aefparams

for i,p in enumerate(all_params):
	filename = "params/param{}.npy".format(i)
	pv = np.random.random(p.get_shape()) * init_scalar
	if load:
		if os.path.isfile(filename):
			pv_stored = np.load(open(filename, 'r'))
			slices = [slice(0, s) for s in pv_stored.shape]
			pv[slices] = pv_stored
	sess.run(p.assign(pv))

def feed_dict(c):
	rand_idx = random.randint(0, len(random_examples) - num_random - 1)
	batch = c + random_examples[rand_idx:rand_idx + num_random]
	batch_indices = [indices[i] for i in batch]

	categories = np.concatenate([np.ones(len(c), dtype=dtype), random_zeros])

	rand_idx1 = random.randint(0, titles.shape[0] - num_disc - 1)
	rand_idx2 = random.randint(0, atitles.shape[0] - num_disc - 1)
	dpos = slice(rand_idx1, rand_idx1 + num_disc)
	dneg = slice(rand_idx2, rand_idx2 + num_disc)

	if aef_only:
		return {aelatent: aelatent_np[batch_indices], target_categories: categories}


	return {input_titles: titles[batch_indices], target_categories: categories, input_dpos: titles[dpos], input_dneg: atitles[dneg], input_mask: mask[batch_indices], input_dpos_mask: mask[dpos], input_dneg_mask: amask[dneg]}

if use_eval:
	eval()

if print_initial_loss:
	losses = []
	dlosses = []
	aelosses = []
	for c in clusters[beg_cluster:end_clusters]:
		feed = feed_dict(c)
		batch_loss, batch_dloss = sess.run([l, dl], feed)
		losses.append(batch_loss)
		dlosses.append(batch_dloss)
	print("initial: {} {}".format(sum(losses) / len(losses), sum(dlosses) / len(dlosses)))

for epoch in range(num_epochs):
	losses = []
	dlosses = []
	aelosses = []
	aeflosses = []
	for j,c in enumerate(clusters[beg_cluster:end_clusters]):
		feed = feed_dict(c)

		prev_params = sess.run(all_params, feed)

		if aef_only:
			_, batch_aefloss = sess.run([aefopt, aefl], feed)
			aeflosses.append(batch_aefloss)
		else:
			_, _, _, _, _, batch_loss, batch_dloss, batch_aeloss, batch_aefloss = sess.run([opt, dopt, aopt, aeopt, aefopt, l, dl, aeloss, aefl], feed)
			losses.append(batch_loss)
			dlosses.append(batch_dloss)
			aelosses.append(batch_aeloss)
			aeflosses.append(batch_aefloss)


		if j % (len(clusters[beg_cluster:end_clusters]) / 100 + 1) == 0:
			sys.stderr.write('.')

		new_params = sess.run(all_params, feed)
		for k in range(len(all_params)):
			if not np.isfinite(new_params[k]).all():
				pdb.set_trace()
				if not np.isfinite(prev_params[k]).all():
					print('error: already nan')
					pdb.set_trace()
				print("undoing nan gradient {}".format(k))
				sess.run(all_params[k].assign(prev_params[k]))

	sys.stderr.write('\n')

	if save:
		for j,pv in enumerate(sess.run(all_params)):
			filename = "params/param{}.npy".format(j)
			filename2 = "params/param{}_{}.npy".format(j, epoch)
			np.save(filename, pv)
			if save_all:
				np.save(filename2, pv)

	if aef_only:
		print("{}: {}".format(epoch, sum(aeflosses) / len(aeflosses)))
	else:
		print("{}: {} {} {} {}".format(epoch, sum(losses) / len(losses), sum(dlosses) / len(dlosses), sum(aelosses) / len(aelosses), sum(aeflosses) / len(aeflosses)))

	if (epoch + 1) % 1 == 0:
		if use_eval:
			eval()

pdb.set_trace()

