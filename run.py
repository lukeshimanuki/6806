import tensorflow as tf
import numpy as np
import os
import pdb
import sys
import math

batch_size = 500
input_features = 'aecfeatures'
dtype = np.float32

titles = np.load("{}.npy".format(input_features))
bodies = np.load('bodies.npy')
mask = np.load('mask.npy')
abodies = np.load('abodies.npy')
atitles = np.load("a{}.npy".format(input_features))
amask = np.load('amask.npy')

import model

def run(titles, bodies, mask, prefix):
	sess = tf.Session()

	input_titles = tf.placeholder(dtype, [None] + list(titles.shape[1:]))
	input_bodies = tf.placeholder(dtype, [None] + list(bodies.shape[1:]))
	input_mask = tf.placeholder(dtype, [None] + list(mask.shape[1:]))
	features, params = model.model(input_titles, input_mask, 1)
	domain, dparams = model.discriminate(features)
	ae_conv, _, aeparams = model.autoencoder(input_titles, input_mask, 1)
	ae = tf.reduce_sum(ae_conv, 1)
	aef, aefparams = model.flat(ae_conv, input_mask, 1, False)

	sess.run(tf.global_variables_initializer())

	all_params = params + dparams + aeparams + aefparams
	for i,p in enumerate(all_params):
		filename = "params/param{}.npy".format(i)
		pv = np.random.random(p.get_shape())
		if os.path.isfile(filename):
			pv = np.load(open(filename, 'r'))
			pass
		sess.run(p.assign(pv))

	out_features = np.zeros([titles.shape[0], features.shape[1]])
	out_aelatent = np.zeros([titles.shape[0], ae.shape[1]])
	out_aefeatures = np.zeros([titles.shape[0], aef.shape[1]])
	out_aecfeatures = np.zeros([titles.shape[0]] + list(ae_conv.shape[1:]), dtype=np.float16)

	for i in range(int(math.ceil(float(titles.shape[0]) / batch_size))):
		s = slice(i * batch_size, (i+1) * batch_size)
		feed = {input_titles: titles[s].reshape([-1] + list(titles.shape[1:])), input_bodies: bodies[s].reshape([-1] + list(bodies.shape[1:])), input_mask: mask[s].reshape([-1] + list(mask.shape[1:]))}

		f = sess.run(features, feed)
		out_features[s] = f

		#aec = sess.run(ae_conv, feed)
		#out_aecfeatures[s] = aec.astype(np.float16)

		#l = sess.run(ae, feed)
		#out_aelatent[s] = l

		#a = sess.run(aef, feed)
		#out_aefeatures[s] = a

	np.save("{}features.npy".format(prefix), out_features.astype(np.float16))
	#np.save("{}aelatent.npy".format(prefix), out_aelatent.astype(np.float16))
	#np.save("{}aefeatures.npy".format(prefix), out_aefeatures.astype(np.float16))
	#np.save("{}aecfeatures.npy".format(prefix), out_aecfeatures)

run(titles, bodies, mask, '')
run(atitles, abodies, amask, 'a')

#pdb.set_trace()

